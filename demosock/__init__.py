from pyramid.config import Configurator


def main(global_config, **settings):
    with Configurator(settings=settings) as config:
        config.include('cornice')
        config.include('demosock.rbow')
        config.scan('demosock.views')
        return config.make_asgi_app()


application = main({})
