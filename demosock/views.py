import logging

from .rbow.websocket import AsyncWebsocketConsumer, SyncWebsocketConsumer
from .rbow import websocket

log = logging.getLogger(__name__)


@websocket('my-websocket')
class MyWebsocket(AsyncWebsocketConsumer):

    async def on_receive(self, data):
        print(f'Received: {data}')
        await self.send('Pong')


@websocket('my-other-websocket')
class MyOtherWebsocket(SyncWebsocketConsumer):

    def on_receive(self, data):
        print(f'Received: {data}')
        self.send('Pong')
