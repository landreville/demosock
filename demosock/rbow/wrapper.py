from .connection import AsgiConnection, AsgiHttpConnection


class AsgiView(object):

    connection_class = AsgiConnection

    def __init__(self, registry, view_callable):
        self._view_callable = view_callable

    def __call__(self, scope):
        async def wrap(receive, send):
            return await self._view_callable(self.connection_class(scope, receive, send))
        return wrap


class AsgiHttpView(AsgiView):
    connection_class = AsgiHttpConnection
