import abc
import logging
from asgiref.sync import async_to_sync, sync_to_async
import asyncio
from ..utils import RequestContext

log = logging.getLogger(__name__)


class BaseConsumer(object, metaclass=abc.ABCMeta):

    def __init__(self, registry, scope, send):
        self.registry = registry
        self.scope = scope
        self.raw_send = send
        self.closed = False

    @abc.abstractmethod
    def dispatch(self, message):
        pass


class AsyncWebsocketConsumer(BaseConsumer):

    async def accept(self):
        await self.raw_send({'type': 'websocket.accept'})

    async def close(self, code=None):
        self.closed = True
        message = {'type': 'websocket.close'}
        if code is not None:
            message['code'] = code
        await self.raw_send(message)

    async def dispatch(self, message):
        if message['type'] == 'websocket.connect':
            await self.on_connect()
        elif message['type'] == 'websocket.receive':
            await self.receive(message)
        elif message['type'] in ('websocket.disconnect', 'websocket.close'):
            await self.on_client_close()

    async def on_connect(self):
        await self.accept()

    async def on_client_close(self):
        self.closed = True

    async def on_receive(self, text_data):
        pass

    async def on_receive_bytes(self, bytes_data):
        pass

    async def receive(self, message):
        text_data = message.get('text')
        if text_data is not None:
            await self.receive(text_data=text_data)
        else:
            bytes_data = message.get('bytes')
            if bytes_data is not None:
                await self.receive(bytes_data=bytes_data)
        if text_data:
            await self.on_receive(text_data)
        elif bytes_data:
            await self.on_receive_bytes(bytes_data)
        else:
            raise ValueError('text_data or bytes_data must be non-None')

    async def send(self, value):
        if isinstance(value, bytes):
            key = 'bytes'
        else:
            key = 'text'

        return await self.raw_send({
            'type': 'websocket.send',
            key: value
        })


class SyncWebsocketConsumer(BaseConsumer):

    def __init__(self, scope, send):
        super().__init__(scope, send)
        self.raw_send = async_to_sync(send)

    def accept(self):
        self.raw_send({'type': 'websocket.accept'})

    def close(self, code=None):
        message = {'type': 'websocket.close'}
        if code is not None:
            message['code'] = code
        self.raw_send(message)
        self.on_close()

    @sync_to_async
    def dispatch(self, message):
        if message['type'] == 'websocket.connect':
            self.on_connect()
        elif message['type'] == 'websocket.receive':
            self.receive(message)
        elif message['type'] in ('websocket.disconnect', 'websocket.close'):
            self.on_client_close()

    def on_connect(self):
        self.accept()

    def on_client_close(self):
        pass

    def on_close(self):
        pass

    def on_receive(self, text_data):
        pass

    def on_receive_bytes(self, bytes_data):
        pass

    def receive(self, message):
        text_data = message.get('text')
        if text_data is not None:
            self.receive(text_data=text_data)
        else:
            bytes_data = message.get('bytes')
            if bytes_data is not None:
                self.receive(bytes_data=bytes_data)

        if text_data:
            self.on_receive(text_data)
        elif bytes_data:
            self.on_receive_bytes(bytes_data)
        else:
            raise ValueError('text_data or bytes_data must be non-None')

    def send(self, value):
        if isinstance(value, bytes):
            key = 'bytes'
        else:
            key = 'text'
            value = value.encode('utf-8')

        return self.raw_send({
            'type': 'websocket.send',
            key: value
        })


class AsyncPollConsumer(AsyncWebsocketConsumer):
    interval = 5

    @abc.abstractmethod
    async def on_interval(self):
        pass

    async def on_connect(self):
        await super().on_connect()
        asyncio.ensure_future(self._start_polling())

    async def _start_polling(self):
        while not self.closed:
            await self.on_interval()
            await asyncio.sleep(self.interval)


class SyncPollConsumer(AsyncWebsocketConsumer):
    interval = 5

    @abc.abstractmethod
    def on_interval(self):
        pass

    def on_connect(self):
        super().on_connect()
        loop = asyncio.get_event_loop()
        asyncio.run_coroutine_threadsafe(self._start_polling, loop)

    async def _start_polling(self):
        async_on_interval = sync_to_async(self.on_interval)

        while not self.closed:
            await async_on_interval()
            await asyncio.sleep(self.interval)


class RequestConsumer(SyncWebsocketConsumer):

    @abc.abstractmethod
    def on_receive(self, request):
        pass

    def receive(self, message):
        with RequestContext(self.registry, self.scope, message) as request:
            resp = self.on_receive(request)
            if resp is not None:
                self.send(resp)
