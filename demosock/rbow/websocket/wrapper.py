from .bridge import WebsocketBridge


class AsgiWebsocket(object):

    def __init__(self, registry, consumer_class):
        self._consumer_class = consumer_class
        self._registry = registry

    def __call__(self, scope):
        return WebsocketBridge(self._registry, self._consumer_class, scope)
