from .consumers import BaseConsumer


class WebsocketBridge(object):

    def __init__(self, registry, consumer_class, scope: dict):
        self._consumer_class = consumer_class
        self._scope = scope
        self._registry = registry

    async def __call__(self, receive, send):
        stay_alive = True

        def disconnect():
            nonlocal stay_alive
            stay_alive = False

        consumer: BaseConsumer = self._consumer_class(
            self._registry,
            self._scope,
            SendWrapper(send, disconnect, disconnect)
        )

        while stay_alive:
            message = await receive()
            if message['type'] in ('websocket.disconnect', 'websocket.close'):
                disconnect()
            await consumer.dispatch(message)


class SendWrapper(object):

    def __init__(self, send, on_disconnect, on_close):
        self._raw_send = send
        self._on_disconnect = on_disconnect
        self._on_close = on_close

    async def __call__(self, value):
        if value['type'] == 'websocket.disconnect':
            self._on_disconnect()
        elif value['type'] == 'websocket.close':
            self._on_close()

        return await self._raw_send(value)

