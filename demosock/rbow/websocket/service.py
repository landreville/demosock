import venusian
import functools
from asgiref.sync import SyncToAsync
from pyramid.interfaces import IRouter
from pyramid.request import Request
from .consumers import AsyncWebsocketConsumer
from .bridge import WebsocketBridge
from ..utils import build_environ


class WebsocketService(object):
    """
    Register Pyramid view-callables as endpoints for websocket actions.

    This simply makes a normal request to the Pyramid app when actions occur on
    the websocket.
    """

    def __init__(self, name, pattern):
        self.name = name
        self.pattern = pattern
        self.route_added = False

        self.receive = functools.partial(WebsocketView, self, 'receive')
        self.poll = functools.partial(WebsocketView, self, 'poll')
        self.event = functools.partial(WebsocketView, self, 'event')


class WebsocketView(object):

    def __init__(self, svc, action, *args, **kwargs):
        self._svc = svc
        self._action = action
        self._args = args
        self._kwargs = kwargs

    def __call__(self, wrapped):
        venusian.attach(wrapped, self._register)

    def _register(self, scanner, name, wrapped):
        config = scanner.config
        action = self._action
        pattern = self._svc.pattern
        registry = config.registry

        if not self._svc.route_added:
            path = '/_rbow/{action}/'
            if pattern.startswith('/'):
                path += pattern[1:]
            else:
                path += pattern

                route_name = f'{self._svc.name}'

            config.add_route(route_name, path)
            self._svc.route_added = True

        config.add_view(
            wrapped,
            *self._args,
            route_name=route_name,
            match_param=f'action={action}',
            *self._kwargs
        )

        scanner.config.add_asgi_view(
            wrapped,
            *self.args,
            protocol='websocket',
            asgi_view_wrapper=AsgiWebsocketViewWrapper,
            **self.kwargs
        )


class AsgiWebsocketViewWrapper(object):

    def __init__(self, registry, path):
        self._registry = registry
        self._path = path

    def __call__(self, scope):
        return WebsocketBridge(
            self._registry,
            functools.partial(
                PyramidViewConsumer,
                path=self._path
            ),
            scope,
        )


class PyramidViewConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, path, **kwargs):
        super().__init__(*args, **kwargs)

        app = self.registry.queryUtility(IRouter)
        self._path = path
        self._invoke_subrequest = SyncToAsync(app.invoke_subrequest)

    async def on_receive(self, text_data):
        request = Request.blank(self._path, build_environ(self.scope))
        resp = await self._invoke_subrequest(request)
        await self.send(resp.body)
